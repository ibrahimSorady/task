---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_86e0ac5d4f8ce9853bc22fd08f2a0109 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET -G "http://localhost/api/products" 
```

```javascript
const url = new URL("http://localhost/api/products");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
[
    {
        "id": 124,
        "name": "latest test",
        "code": "3b5f1de0-2024-11e9-89e2-d34ce91689f3",
        "price": 10000,
        "created_at": "2019-01-24 22:05:59",
        "updated_at": "2019-01-24 22:05:59"
    },
    {
        "id": 123,
        "name": "newest test",
        "code": "23eebfa0-2024-11e9-8acc-6fe8a3f22857",
        "price": 129,
        "created_at": "2019-01-24 22:05:20",
        "updated_at": "2019-01-24 22:05:20"
    },
    {
        "id": 122,
        "name": "new test",
        "code": "e3cf8fe0-2023-11e9-9d5e-83fabe11421c",
        "price": 99,
        "created_at": "2019-01-24 22:03:32",
        "updated_at": "2019-01-24 22:03:32"
    },
    {
        "id": 121,
        "name": "New Test Product",
        "code": "2f491500-2023-11e9-81ac-3b67bf069dff",
        "price": 120,
        "created_at": "2019-01-24 21:58:30",
        "updated_at": "2019-01-24 21:58:30"
    },
    {
        "id": 25,
        "name": "Kelly Christiansen II",
        "code": "VqJu51vcI4jU78U71BQ5FnSWAKnVJ4CNBiCR",
        "price": 4,
        "created_at": "2019-01-21 20:28:02",
        "updated_at": "2019-01-21 20:28:02"
    },
    {
        "id": 24,
        "name": "Mrs. Alexa Schaden",
        "code": "7I69IZY7gjKEBmC1PI87qmA7EH1vwBOROmKo",
        "price": 2,
        "created_at": "2019-01-21 20:28:01",
        "updated_at": "2019-01-21 20:28:01"
    },
    {
        "id": 23,
        "name": "Malachi Shields",
        "code": "w11HYOwAK88VfvNGl96jALtmFmVRvbbQfAmH",
        "price": 2,
        "created_at": "2019-01-21 20:28:01",
        "updated_at": "2019-01-21 20:28:01"
    },
    {
        "id": 22,
        "name": "Dr. Daisy Hand",
        "code": "y2TtqV4mEA07UPL7NT4ssric7lBMGWQOjkVG",
        "price": 3,
        "created_at": "2019-01-21 20:28:01",
        "updated_at": "2019-01-21 20:28:01"
    },
    {
        "id": 21,
        "name": "Camilla Tremblay",
        "code": "Iss9sl8RkD3rfV8fPm7qbPvtdsfJ2E1VpIvz",
        "price": 2,
        "created_at": "2019-01-21 20:28:01",
        "updated_at": "2019-01-21 20:28:01"
    },
    {
        "id": 20,
        "name": "Mr. Ethan Nienow",
        "code": "U8cYwD6kCDyLxyRHB0l44qsUuFdtzOzuPesQ",
        "price": 5,
        "created_at": "2019-01-21 20:28:01",
        "updated_at": "2019-01-21 20:28:01"
    },
    {
        "id": 19,
        "name": "Glenna Kunde",
        "code": "7O4X0BnG5bBIaQUX0oTYwG2TMTwFiZGnTxC9",
        "price": 2,
        "created_at": "2019-01-21 20:28:01",
        "updated_at": "2019-01-21 20:28:01"
    },
    {
        "id": 18,
        "name": "Prof. Osvaldo Becker",
        "code": "iFATVhb3TqywNvb3rPduVBbo48NESeWhu2mM",
        "price": 3,
        "created_at": "2019-01-21 20:28:01",
        "updated_at": "2019-01-21 20:28:01"
    },
    {
        "id": 17,
        "name": "Tyson Turcotte",
        "code": "AR6BU63JcInYEbmqNMYxT5iuKWYOHfiAv5PI",
        "price": 4,
        "created_at": "2019-01-21 20:28:01",
        "updated_at": "2019-01-21 20:28:01"
    },
    {
        "id": 16,
        "name": "Prof. Myrtle Bechtelar PhD",
        "code": "QmTtph1BQ5YxRAu8E0EJVuErHFLIHChlY2dU",
        "price": 4,
        "created_at": "2019-01-21 20:28:01",
        "updated_at": "2019-01-21 20:28:01"
    },
    {
        "id": 15,
        "name": "Orval VonRueden",
        "code": "QA1JZ8qfTUwnlVFkloSXnTkIXvD2ov9r4PDd",
        "price": 2,
        "created_at": "2019-01-21 20:28:01",
        "updated_at": "2019-01-21 20:28:01"
    },
    {
        "id": 14,
        "name": "Kathlyn Torphy",
        "code": "90G4klNqQMCWP2fJVJ8fv9YhctTxqwSsSDVl",
        "price": 2,
        "created_at": "2019-01-21 20:28:01",
        "updated_at": "2019-01-21 20:28:01"
    },
    {
        "id": 13,
        "name": "Ms. Myra Wehner MD",
        "code": "A4BThtvWX9qJTbMBCKpz8El3iBPxuLdjs2aU",
        "price": 4,
        "created_at": "2019-01-21 20:28:01",
        "updated_at": "2019-01-21 20:28:01"
    },
    {
        "id": 12,
        "name": "Retta Mraz",
        "code": "MlJY5MyZvviITOZxEBwYcHUN8RRJRJV52mMJ",
        "price": 2,
        "created_at": "2019-01-21 20:28:01",
        "updated_at": "2019-01-21 20:28:01"
    },
    {
        "id": 11,
        "name": "Kiera Abbott",
        "code": "jdUyavPK8KSOEiMqsvixmxtHqLhDJsbWD2mX",
        "price": 3,
        "created_at": "2019-01-21 20:28:01",
        "updated_at": "2019-01-21 20:28:01"
    },
    {
        "id": 9,
        "name": "Ivory Ullrich Jr.",
        "code": "kgyFYGzKkeQDcqo2KdGqBMayJYtGvn85t8oo",
        "price": 5,
        "created_at": "2019-01-21 20:28:01",
        "updated_at": "2019-01-21 20:28:01"
    },
    {
        "id": 8,
        "name": "Dr. Theo Upton II",
        "code": "SQdIlXmxITbEYb2o9lzOPCIY5tB2H8dLKmNA",
        "price": 5,
        "created_at": "2019-01-21 20:28:01",
        "updated_at": "2019-01-21 20:28:01"
    },
    {
        "id": 7,
        "name": "Hollie Cartwright",
        "code": "DAYkPNqi1uDXljfrn1G1ebKbfUao05v4Yk4E",
        "price": 5,
        "created_at": "2019-01-21 20:28:01",
        "updated_at": "2019-01-21 20:28:01"
    },
    {
        "id": 6,
        "name": "Brady Friesen",
        "code": "eOIWuAkB2ixLhgb7RBlk7KNS8XSWis594dmb",
        "price": 4,
        "created_at": "2019-01-21 20:28:00",
        "updated_at": "2019-01-21 20:28:00"
    },
    {
        "id": 5,
        "name": "Dr. Ransom Bashirian I",
        "code": "BYUwvSP2ccjHEL47Z2PQKqK34XPirPt2qfRa",
        "price": 5,
        "created_at": "2019-01-21 20:28:00",
        "updated_at": "2019-01-21 20:28:00"
    },
    {
        "id": 4,
        "name": "Tyson Dickens",
        "code": "S6ofFibHMp9Mtmoqh916sSNjvJG85yHefxiq",
        "price": 4,
        "created_at": "2019-01-21 20:28:00",
        "updated_at": "2019-01-21 20:28:00"
    },
    {
        "id": 3,
        "name": "Dr. Oceane Nitzsche",
        "code": "PahLOskGmHJ65MQRHb6vkQs44h7tOZfdTlMl",
        "price": 3,
        "created_at": "2019-01-21 20:28:00",
        "updated_at": "2019-01-21 20:28:00"
    },
    {
        "id": 2,
        "name": "Joelle Kiehn",
        "code": "EI8yxKBGgbzHis55zdNUm5IYFJzQ4wLvmIEK",
        "price": 4,
        "created_at": "2019-01-21 20:28:00",
        "updated_at": "2019-01-21 20:28:00"
    },
    {
        "id": 1,
        "name": "product3",
        "code": "sadahdsjlkasjd",
        "price": 120,
        "created_at": "2019-01-21 20:28:00",
        "updated_at": "2019-01-21 22:00:51"
    }
]
```

### HTTP Request
`GET api/products`


<!-- END_86e0ac5d4f8ce9853bc22fd08f2a0109 -->

<!-- START_92097916400bd214580a78ae6b2a0855 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET -G "http://localhost/api/product/{id}" 
```

```javascript
const url = new URL("http://localhost/api/product/{id}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "id": 1,
    "name": "product3",
    "code": "sadahdsjlkasjd",
    "price": 120,
    "created_at": "2019-01-21 20:28:00",
    "updated_at": "2019-01-21 22:00:51"
}
```

### HTTP Request
`GET api/product/{id}`


<!-- END_92097916400bd214580a78ae6b2a0855 -->

<!-- START_469fd283a01ebdb6b4556724ea329fd4 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://localhost/api/product/{id}" 
```

```javascript
const url = new URL("http://localhost/api/product/{id}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`PUT api/product/{id}`


<!-- END_469fd283a01ebdb6b4556724ea329fd4 -->

<!-- START_b70a4429b13f5218b82577ad923326e7 -->
## Show the form for creating a new resource.

> Example request:

```bash
curl -X POST "http://localhost/api/product/create" 
```

```javascript
const url = new URL("http://localhost/api/product/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/product/create`


<!-- END_b70a4429b13f5218b82577ad923326e7 -->

<!-- START_180d60b6481f5b1787ffcdb42da65460 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://localhost/api/product/{id}" 
```

```javascript
const url = new URL("http://localhost/api/product/{id}");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE api/product/{id}`


<!-- END_180d60b6481f5b1787ffcdb42da65460 -->


