<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/',function (){
    return 'am here';
});

Route::get('products', 'ProductController@index');
Route::get('product/{id}', 'ProductController@show');
Route::put('product/{id}', 'ProductController@update');
Route::post('product/create', 'ProductController@create');
Route::delete('product/{id}', 'ProductController@destroy');