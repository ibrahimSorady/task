<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products = Product::orderBy('id', 'DESC')->get();
        if (count($products) == 0){
            return response()->json('No Products Found');
        }
        return response()->json($products,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:30',
            'code' => 'uuid',
            'price' => 'required|numeric|min:1',
        ]);;
        if ($validator->fails()) {
            $response = array('response' => $validator->messages(), 'success' => false);
            return $response;
        } else {
            $product = new Product;
            $product->name = $request->name;
            $product->code = Uuid::generate()->string;
            $product->price = $request->price;
            $product->save();
            return response()->json($product,201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        if (!$product){
            return response()->json('Error', 'No Products Found');
        }
        return response()->json($product,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product = Product::findOrFail($request->id);
        $product->name = $request->name;
        $product->code = $request->code;
        $product->price = $request->price;
        $product->save();
        return response()->json($product,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        return response()->json([
            'message' => 'Product is deleted'
        ], 204);
    }
}
